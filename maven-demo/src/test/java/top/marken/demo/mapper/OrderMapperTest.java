package top.marken.demo.mapper;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import top.marken.demo.entity.Order;
import java.util.Date;

@ContextConfiguration(value = "classpath:springApplication.xml")
@RunWith(SpringJUnit4ClassRunner.class)
public class OrderMapperTest {
    ApplicationContext context = new ClassPathXmlApplicationContext("springApplication.xml");
    SqlSessionFactory factory = context.getBean(SqlSessionFactoryBean.class).getObject();

    public OrderMapperTest() throws Exception {
    }

    @Test
    public void findOrders() {
        SqlSession sqlSession = factory.openSession();
        OrderMapper mapper = sqlSession.getMapper(OrderMapper.class);
        Order order = new Order();
        order.setuId(4);
        order.setName("葡萄");
        order.setPrice(23D);
        mapper.insertOne(order);
        System.out.println(order);
    }

    @Test
    public void updateOne() {
        SqlSession sqlSession = factory.openSession();
        OrderMapper mapper = sqlSession.getMapper(OrderMapper.class);
        Order order = new Order();
        order.setId(1);
        order.setTime(new Date());
        mapper.updateOne(order);
    }
}
