package top.marken.demo.mapper;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import top.marken.demo.entity.User;

@ContextConfiguration(value = "classpath:springApplication.xml")
@RunWith(SpringJUnit4ClassRunner.class)
public class UserMapperTest {
    ApplicationContext context = new ClassPathXmlApplicationContext("springApplication.xml");
    SqlSessionFactory factory = context.getBean(SqlSessionFactoryBean.class).getObject();

    public UserMapperTest() throws Exception {
    }

    @Test
    public void findAll() {
        OrderMapper orderMapper = context.getBean(OrderMapper.class);
        System.out.println(orderMapper.findOrdersByUId(1));
    }

    @Test
    public void sessionTest() {
        SqlSession sqlSession = factory.openSession();
        UserMapper mapper = sqlSession.getMapper(UserMapper.class);
        User user = mapper.findByUsername("admin");
        System.out.println(user);
    }
}
