package top.marken.demo.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import top.marken.demo.mapper.OrderMapper;
import top.marken.demo.entity.Order;


import java.util.List;

@ContextConfiguration(value = "classpath:springApplication.xml")
@RunWith(SpringJUnit4ClassRunner.class)
public class UserServiceTest {
    ApplicationContext context = new ClassPathXmlApplicationContext("springApplication.xml");

    @Test
    public void findAll() {
        OrderMapper orderMapper = context.getBean(OrderMapper.class);
        /*for (Order order : orderDao.findOrdersByUId(1)) {
            System.out.println(order);

        }*/

        // System.out.println(orderDao.getTotalByUId(1));

        List<Order> orders = orderMapper.getPageOrdersByUId(1, 2, 10);
        System.out.println(orders);
    }
}
