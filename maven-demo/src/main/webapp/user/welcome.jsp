<%--
  Created by IntelliJ IDEA.
  User: Marken
  Date: 2021/9/9
  Time: 15:40
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <title>主页面</title>
    <jsp:include page="/user/include/head.jsp"/>
    <style>
        * {
            padding: 0;
            margin: 0;
        }

        .view table {
            border: 1px solid;
            margin: 0 auto;
        }

        .view h3 {
            text-align: center;
        }

        .view tr td {
            border: 1px solid;
        }

        nav>div>span {
            float: right;
        }
    </style>

    <script>
        const SIZE = 10

        window.onload = init
        function init() {
            getOrder(show,1,SIZE)
        }

        function changePage() {
            var pageNum = document.getElementById('pageNum').value
            var sizeNum = document.getElementById('sizeNum').value
            if (pageNum === '' || sizeNum === '') return alert('请选择页数，和每页数量')
            getOrder(show,Number(pageNum),Number(sizeNum));
        }

        function showPageBtns(total,size) {
            var pageBtns = document.getElementById('pageBtns')
            pageBtns.innerHTML = ''
            var button
            for (let i = 1; i <= total; i++) {
                button = document.createElement('button')
                button.innerText = i
                button.className = 'button btn-success'
                button.onclick = ()=> {
                    getOrder(show,i,size)
                }
                pageBtns.appendChild(button)
            }
        }

        function show(pageOrder,size) {
            var rows = pageOrder.rows
            var total = pageOrder.total
            showPageBtns(total,size)
            var showArea = document.getElementById('showArea')
            showArea.innerHTML = ''

            var cnt = 1;
            for (let order of rows) {
                var tr = document.createElement('tr')
                var td = document.createElement('td')
                td.innerText = cnt
                tr.appendChild(td)

                td = document.createElement('td')
                td.innerText = order.name
                tr.appendChild(td)

                td = document.createElement('td')
                td.innerText = order.price
                tr.appendChild(td)

                td = document.createElement('td')
                td.innerText = order.time
                tr.appendChild(td)

                td = document.createElement('td')
                var a = document.createElement('a')
                a.className = 'button btn-primary'
                a.href = '<c:url value="/user/update_page/order/"/>' + order.id
                a.innerText = '修改'
                td.appendChild(a)
                var button = document.createElement('button')
                button.className = 'button btn-danger'
                button.onclick = function() {
                    deleteAjax(order.id)
                }
                button.innerText = '删除' + order.id
                td.appendChild(button)
                tr.appendChild(td)

                showArea.appendChild(tr)
                cnt++
            }
        }

        function deleteAjax(orderId) {
            alert(orderId)
            $.ajax({
                type: 'post',
                url: '<c:url value="/user/delete/order/ajax"/>',
                data: JSON.stringify({
                    id:orderId
                }),
                /* 提交数据的类型 */
                contentType: 'application/json',
                /* 获取数据的类型 */
                dataType: 'json',
                success: (data)=> {
                    if (data.code === 0) {
                        window.location.href = '<c:url value="/user/welcome"/>'
                    }
                    console.log(data.msg)
                }
            })
        }

        function getOrder(show,page,size) {
            $.ajax({
                type: 'post',
                url: '<c:url value="/user/order/page/ajax"/>',
                data: JSON.stringify({
                    page:page,
                    size:size
                }),
                /* 提交数据的类型 */
                contentType: 'application/json',
                /* 获取数据的类型 */
                dataType: 'json',
                success: (data)=> {
                    if (data.code === 0) {
                        show(data.data,size);
                    }
                    console.log(data.msg)
                }
            })
        }

        function seeJson() {
            $.ajax({
                type: 'post',
                url: '<c:url value="/user/welcome.json"/>',
                data: {},
                /* 提交数据的类型 */
                contentType: 'application/json',
                /* 获取数据的类型 */
                dataType: 'json',
                success: (data)=> {
                    console.log(data);
                }
            })
        }
    </script>
</head>
<body>
    <nav>
        <h2>登陆成功</h2>
        <div>
            欢迎你 ${user.name}
            <span>
                <a href="<c:url value="/user/login/out"/>">退出登录</a>
            </span>
        </div>
        <a href="<c:url value="/user/order_page"/>">我要下单</a>
    </nav>

    <div class="view">
        <h3>订单列表</h3>
        <button onclick="seeJson()">查看JSON</button>
        <table>
            <tr>
                <td>#</td>
                <td>商品名称</td>
                <td>支付金额</td>
                <td>交易时间</td>
                <td>操作</td>
            </tr>
            <tbody id="showArea">

            </tbody>
        </table>

        <div>
            <div>
                <label for="pageNum">页数</label>
                <input type="number" id="pageNum"/>
                <label for="sizeNum">每页行数</label>
                <input type="number" id="sizeNum"/>
                <button type="button" onclick="changePage()">点击换页</button>
            </div>
            <div id="pageBtns">

            </div>
        </div>
    </div>
</body>
</html>
