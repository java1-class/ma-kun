<%--
  Created by IntelliJ IDEA.
  User: Marken
  Date: 2021/9/13
  Time: 10:45
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>file上传</title>
    <style>
        * {
            padding: 0;
            margin: 0;
        }

        table {
            margin: 100px auto;
            border: 1px solid;
        }

        td {
            border: 1px solid;
        }

        button {
            width: 100%;
        }
    </style>
</head>
<body>
<form method="post" action="<c:url value="/user/file/put"/>" enctype="multipart/form-data">
    <table>
        <tr>
            <td>选择文件</td>
            <td><input type="file" name="imgPath"/></td>
        </tr>
        <tr>
            <td colspan="2">
                <button type="submit">点击上传</button>
            </td>
        </tr>
    </table>
</form>
</body>
</html>
