<%--
  Created by IntelliJ IDEA.
  User: Marken
  Date: 2021/9/9
  Time: 11:44
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>登录页面</title>
    <jsp:include page="/user/include/head.jsp"/>
</head>
<body>
  <form method="post" class="row-cols-4"  action="<c:url value="/user/check"/>">
      <div class="form-group">
          <label for="username">登录名</label>
          <input type="text" class="form-control" id="username" name="username" aria-describedby="usernameHelp">
          <small id="usernameHelp" class="form-text text-muted">说明</small>
      </div>
      <div class="form-group">
          <label for="password">密码</label>
          <input type="password" class="form-control" id="password" name="password">
      </div>
      <button type="submit" class="btn btn-primary form-control">登录</button>
  </form>
</body>
</html>
