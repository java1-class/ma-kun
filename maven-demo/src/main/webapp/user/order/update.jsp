<%--
  Created by IntelliJ IDEA.
  User: Marken
  Date: 2021/9/13
  Time: 16:37
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <title>修改页面</title>
    <jsp:include page="/user/include/head.jsp"/>
</head>
<script>
    function update() {
        $.ajax({
            type: 'post',
            url: '<c:url value="/user/update/order/ajax"/>',
            data: JSON.stringify({
                id: ${orderUpdate.id},
                name: $('#name').val(),
                price: Number($('#price').val()),
                time: new Date($('#time').val())
            }),
            /* 提交数据的类型 */
            contentType: 'application/json',
            /* 获取数据的类型 */
            dataType: 'json',
            success: (data)=> {
                if (data.code === 0) {
                    window.location.href = '<c:url value="/user/welcome"/>'
                }
                console.log(data.msg)
            }
        })
    }
</script>
<body>
    <div>
        <%--
        <form action="<c:url value="/user/update/order"/>" method="post">
        --%>
        <form>
            <table>
                <tr>
                    <td>订单名称</td>
                    <td><input type="text" id="name" name="name" value="${orderUpdate.name}"/></td>
                </tr>
                <tr>
                    <td>订单价格</td>
                    <td><input type="text" id="price" name="price" value="${orderUpdate.price}"/></td>
                </tr>
                <tr>
                    <td>下单时间</td>
                    <td><input type="datetime" id="time" name="time" value="<fmt:formatDate value="${orderUpdate.time}" pattern="yyyy-MM-dd HH:mm"/>"/></td>
                </tr>
                <tr>
                    <td colspan="2"><button type="button" onclick="update()">修改订单</button></td>
                </tr>
            </table>
        </form>
    </div>
</body>
</html>
