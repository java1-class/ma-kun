<%--
  Created by IntelliJ IDEA.
  User: Marken
  Date: 2021/9/14
  Time: 9:45
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>下单页面</title>
    <jsp:include page="/user/include/head.jsp"/>
</head>
<script>
    function order() {
        $.ajax({
            type:'post',
            url: '<c:url value="/user/order/ajax"/>',
            data: JSON.stringify({
                name: $('#name').val(),
                price: Number($('#price').val())
            }),
            contentType: 'application/json',
            dataType: 'json',
            success: (data)=> {
                if (data.code === 0) {
                    window.location.href = '<c:url value="/user/welcome"/>'
                }
                console.log(data.msg)
            }
        })
    }
</script>
<body>
<div>
    <%--
    <form action="<c:url value="/user/order"/>" method="post">
    --%>
    <form>
        <table>
            <tr>
                <td>订单名称</td>
                <td><input type="text" id="name" name="name" /></td>
            </tr>
            <tr>
                <td>订单价格</td>
                <td><input type="text" id="price" name="price" /></td>
            </tr>
            <tr>
                <td colspan="2"><button type="button" onclick="order()">提交订单</button></td>
            </tr>
        </table>
    </form>
</div>
</body>
</html>
