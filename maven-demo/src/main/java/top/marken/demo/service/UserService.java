package top.marken.demo.service;

import top.marken.demo.entity.Order;
import top.marken.demo.entity.User;
import top.marken.demo.util.Page;

import java.util.List;

public interface UserService {
    /**
     * 登录验证
     * @param username
     * @param password
     * @return
     */
    boolean login(String username, String password);

    /**
     * 通过username查找用户全部信息
     * @param username
     * @return
     */
    User userInfo(String username);

    /**
     * 查找一个用户的所有订单
     * @param uId
     * @return
     */
    List<Order> userOrders(int uId);

    /**
     * 下单
     * @param order
     * @return
     */
    boolean doOrder(Order order);

    /**
     * 更新一条订单
     * @param order
     * @return
     */
    boolean updateOrder(Order order);

    /**
     * 删除一条订单
     * @param id
     * @return
     */
    boolean deleteOrder(int id);

    /**
     * 获取一条订单信息
     * @param id
     * @return
     */
    Order getOrder(int id);

    /**
     * 下单
     * @param name
     * @param price
     * @param uId
     * @return
     */
    boolean order(String name, double price, int uId);

    /**
     * 分页
     * @param uId
     * @param page
     * @param size
     * @return
     */
    Page<Order> userOrdersPage(int uId, int page, int size);
}
