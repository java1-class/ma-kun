package top.marken.demo.entity;

import org.springframework.format.annotation.DateTimeFormat;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;

@XmlRootElement
public class Order {
    private Integer id;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
    private Date time;
    private Double price;
    private Integer uId;
    private String name;

    public Order() {}

    public Order(Integer id, Date time, Double price, Integer uId, String name) {
        this.id = id;
        this.time = time;
        this.price = price;
        this.uId = uId;
        this.name = name;
    }

    @Override
    public String toString() {
        return "Order{" +
                "id=" + id +
                ", time=" + time +
                ", price=" + price +
                ", uId=" + uId +
                ", name='" + name + '\'' +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Integer getuId() {
        return uId;
    }

    public void setuId(Integer uId) {
        this.uId = uId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
