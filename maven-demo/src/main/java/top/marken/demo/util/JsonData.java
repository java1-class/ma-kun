package top.marken.demo.util;

public class JsonData {
    public static final int CODE_SUCCESS = 0;
    public static final int CODE_FIELD = 1;
    public static final int CODE_ERROR = -1;

    private int code;
    private Object data;
    private String msg;

    public JsonData() {}

    public JsonData(int code, Object data, String msg) {
        this.code = code;
        this.data = data;
        this.msg = msg;
    }

    public static JsonData buildSuccess(Object data, String msg) {
        return new JsonData(CODE_SUCCESS,data,msg);
    }

    public static JsonData buildField(String msg) {
        return new JsonData(CODE_FIELD,null,msg);
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
