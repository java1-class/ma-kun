package top.marken.demo.mapper;

import org.apache.ibatis.annotations.Param;
import top.marken.demo.entity.User;
import java.util.List;

public interface UserMapper {
    List<User> findAll();

    User findById(@Param("id") int id);

    User findByUsername(@Param("username") String username);

    boolean insertOne(User user);

    boolean updateOne(User user);

    boolean deleteOne(@Param("id") int id);
}
