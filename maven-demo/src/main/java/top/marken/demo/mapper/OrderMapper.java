package top.marken.demo.mapper;

import org.apache.ibatis.annotations.Param;
import top.marken.demo.entity.Order;

import java.util.List;

public interface OrderMapper {
    /**
     * 通过用户ID查找所有的订单
     * @param uId
     * @return
     */
    List<Order> findOrdersByUId(@Param("u_id") int uId);

    /**
     * 新增一条订单记录
     * @param order
     * @return
     */
     boolean insertOne(Order order);

    /**
     * 通过一个订单ID查找一条订单
     * @param id
     * @return
     */
    Order findOrderById(@Param("id") int id);

    /**
     * 通过一个订单ID删除一条订单
     * @param id
     * @return
     */
    boolean deleteOne(@Param("id") int id);

    /**
     * 更新一条订单
     * @param order
     * @return
     */
    boolean updateOne(Order order);

    /**
     * 查询该用户有多少个订单
     * @param uId
     * @return
     */
    int getTotalByUId(@Param("u_id") int uId);

    /**
     * 查询该用户的分页订单
     * @param uId 需要查询的用户
     * @param start 开始行数
     * @param size 每页有多少行
     * @return
     */
    List<Order> getPageOrdersByUId(@Param("u_id") int uId, @Param("start") int start, @Param("size") int size);

    /**
     * 匹配查询
     * @param order
     * @return
     */
    List<Order> findOrders(Order order);

    List<Order> findOrders2(Order order);
}
